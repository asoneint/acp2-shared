<?php

class Album_model extends LMS_Model {
	var $table  = 'albums';
	var $fields = array('id',
		'is_pushed',
		'last_pushed',
		'owner_type',
		'owner_id',
		'cover_id',
		'title',
		'num_photo',
		'is_live',
		'create_date', 'create_by', 'create_by_id',
		'modify_date', 'modify_by', 'modify_by_id',
	);

	var $fields_details = array(
		'id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'pk'         => TRUE
		),
		'is_live' => array(
			'type'       => 'INT',
			'pk'         => TRUE,
			'constraint' => 1,
			'default'=>'0',
		),
		'is_pushed' => array(
			'type'       => 'INT',
			'constraint' => 1,
		),
		'last_pushed' => array(
			'type'       => 'DATETIME',
			'null'		 => TRUE,
		),
		'owner_type' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'owner_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'cover_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'title' => array(
			'type'       => 'VARCHAR',
			'constraint' => 100,
		),
		'num_photo' => array(
			'type'       => 'INT',
			'constraint' => 11,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);

	var $child_table = 'albums_photos';

	var $auto_increment = false;
	var $use_guid       = true;

	function remove($album_ids) {

		$this->db->where_in('id', $album_ids);
		$this->db->delete($this->table);

		$this->db->where_in('album_id', $album_ids);
		$this->db->delete($this->child_table);
	}

}
