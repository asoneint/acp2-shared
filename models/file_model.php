<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

class File_model extends LMS_Model {
	var $table = 'files';

	var $fields = array('id',
		'owner_type',
		'owner_id',
		'folder', 'ref_table', 'ref_id', 'name', 'description', 'sys_name', 'file_name', 'file_ext', 'create_date',  'create_by', 'create_by_id', 'modify_date', 'modify_by', 'modify_by_id');

	var $auto_increment = false;
	var $use_guid       = true;
	var $mapping_field  = array('id');
	var $fields_details = array(
		'id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
			'pk'         => TRUE
		),
		'owner_type' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'owner_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'folder' => array(
			'type'       => 'VARCHAR',
			'constraint' => 100,
		),
		'ref_table' => array(
			'type'       => 'VARCHAR',
			'constraint' => 100,
		),
		'ref_id' => array(
			'type'       => 'VARCHAR',
			'constraint' => 36,
		),
		'name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'sys_name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'description' => array(
			'type' => 'TEXT',
			'null' => true,
		),
		'file_name' => array(
			'type'       => 'VARCHAR',
			'constraint' => 200,
		),
		'file_ext' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'create_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'create_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'create_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
		'modify_date' => array(
			'type' => 'DATETIME',
			'null' => TRUE,
		),
		'modify_by' => array(
			'type'       => 'VARCHAR',
			'constraint' => 40,
		),
		'modify_by_id' => array(
			'type'       => 'BIGINT',
			'constraint' => 20,
		),
	);


}