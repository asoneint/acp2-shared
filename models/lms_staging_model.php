<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');
}

/**
 * "LMS_Protected_Model" Model Class for CodeIgniter
 * @author      Leman Kwok
 * @copyright   Copyright (c) 2013, LMSWork.
 * @license     http://codeigniter.com/user_guide/license.html
 * @link        http://lmswork.com
 * @since       Version 1.0
 * @filesource
 *
 */

class LMS_Protected_Model extends LMS_Model {
	var $staging_support     = false;
	var $staging_prefix      = 'staging_';
	var $staging_field       = 'staging_action';
	var $staging_option_name = 'staging';

	protected function save_pre_data($data, $is_insert = true, $options = false) {
		$sql_data = parent::prepare_save_data($data, $is_insert, $options);

		// for staging table
		// one or more fields may exist from $data
		if (!isset($options[$this->staging_option_name]) && $this->staging_support) {

			if (isset($data[$this->staging_field])) {
				$sql_data[$this->staging_field] = $data[$this->staging_field];
			}
		}
		return $sql_data;
	}

	public function publish($arg0, $country_id = false, $lang_code = false, $updated = false) {
		$this->_init_db();

		$record_id = NULL;
		$id_field  = $this->_field($this->pk_field, false);
		// if the first parameter is an array
		// we assume that is where query options for reading an entry
		if (is_array($arg0)) {
			$options = $arg0;
		} else {
			$record_id = $arg0;
			$options   = array($id_field => $arg0);
		}

		$this->selecting_options($options);
		$whereCase = $this->get_where_case(FALSE);
		$this->_selecting_clear();

		$record = $this->read($options);
		if (is_array($arg0)) {
			if (is_null($record) || !is_array($record)) {
				return FALSE;
			}
		} else {
			if (!isset($record[$id_field]) || $record[$id_field] != $record_id) {
				return FALSE;
			}
		}

		$pds_options = array_merge($options, array('pds' => true));
		// read production data
		$pds_record = $this->read($pds_options);

		$table     = $this->_table(true);
		$pds_table = $this->_table(false);

		$new_entry = false;
		$skip_keys = array();
		if (is_array($arg0)) {
			if (is_null($pds_record) || !is_array($pds_record)) {
				$new_entry = true;
			} else {
				$skip_keys = array_keys($options);
			}
		} else {
			if (!isset($pds_record[$id_field]) || $pds_record[$id_field] != $record[$id_field]) {
				$new_entry = true;
			} else {
				$skip_keys = array_keys($options);
			}
		}

		foreach ($record as $key => $val) {
			if (in_array($key, $skip_keys)) {continue;
			}

			$sql_data[$key] = $val;
			if (isset($updated[$key])) {
				$sql_data[$key] = $updated[$key];
			}
		}

		if ($new_entry) {
			// insert into production table
			$this->db->insert($pds_table, $sql_data);

		} else {
			// update record by last update data
			// except record-id, country_id & lang_code
			// copy last update data to new array

			// update production table
			$this->db->update($pds_table, $sql_data);
		}

		// read production data again
		$pds_record = $this->read($pds_options);

		// if record does not found after insert statement or record id is not matched with staging table
		// return false
		if (is_array($record_id)) {
			if (!isset($pds_record[$id_field])) {
				return FALSE;
			}
			if ($pds_record[$id_field] != $record_id) {
				return FALSE;
			}
		}

		// update staging table
		$record = $this->db->update($table,
			array(
				$this->staging_field => NULL,
				'modify_date'        => time_to_date()
			),
			$whereCase
		);

		return TRUE;
	}

	public function unpublish($arg0, $country_id = false, $lang_code = false) {
		if (!isset($this->db)) {$this->load->database();
		}

		$record_id = NULL;
		$id_field  = $this->_field('id', false);
		// if the first parameter is an array
		// we assume that is where query options for reading an entry
		if (is_array($arg0)) {
			$options = $arg0;
		} else {
			$record_id = $arg0;
			$options   = array($id_field => $arg0);
		}

		$this->selecting_options($options);

		$record = $this->read($options);

		if (is_array($arg0)) {
			if (is_null($record) || !is_array($record)) {
				return FALSE;
			}
		} else {
			if (!isset($record[$id_field]) || $record[$id_field] != $record_id) {
				return FALSE;
			}
		}

		$pds_options = array_merge($options, array($this->staging_option_name => false));

		// delete record in production table
		$rst = $this->delete($pds_options);

		if ($rst) {
			$this->save(array($this->staging_field => 'unpublished'), isset($record[$id_field]) ? $record[$id_field] : NULL, $options);
		}

		return $rst;
	}

	protected function selecting_options($options = false, $cache = false) {

		if (isset($options[$this->staging_field]) && $this->staging_support) {
			$this->_where_match($this->staging_field, $options[$this->staging_field], $options);
		}

		parent::selecting_options($options, $cache);
	}

	protected function _table($options = true, $table_name = false) {
		$use_staging                   = isset($options[$this->staging_option_name]) && $options[$this->staging_option_name];
		if (!$table_name) {$table_name = $this->table;
		}

		return $this->db->dbprefix(($this->staging_support && $use_staging ? $this->staging_prefix : '') . $table_name);
	}

}
