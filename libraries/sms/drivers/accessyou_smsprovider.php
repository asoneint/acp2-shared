<?php


class AccessYou_SMSProvider extends SMSProvider
{
	public function getProviderId(){
		return 'com.accessyou.sms';
	}
	
	public function send( $message){
		$data = NULL;
		
		$p = array(
				'accountno'=>$this->_username,
				"pwd"=>$this->_password,
				"msg"=>$message->content,
				'phone'=> $message->countryCode. $message->to,
				'size'=>'l',
				'from'=>$message->from,
		);

		$uri = $this->getSendMessageURI($p);
		
		$CI = &get_instance();
		$CI->load->library('curl');
		
		log_message('debug','AccessYou_SMSProvider/send, $uri = '.$uri);
		$res = new SMSMessageResponse;
		$res->responseString = (string) $CI->curl->simple_get($uri);
		$res->provider = $this->getProviderId();
		log_message('debug','AccessYou_SMSProvider/send, responseString = '.nl2br(htmlentities($res->responseString)));
		
		if(preg_match('#^[0-9]+$#', $res->responseString)){
			$res->identifier = $res->responseString;
			$res->status = 'sent';

		}else{
			$res->status = 'fail';
			$res->identifier =  NULL;
			$res->reason = (string) $res->responseString;
			log_message('error','AccessYou_SMSProvider/send, Reason='.$res->reason);
		}
		
		return $res;
	}

	public function checkStatus($identifier = NULL){
		
		$res = new SMSMessageResponse;
		$res->raw = '';
		if(is_object($identifier)){
			$res = $identifier;
			$identifier = $res->identifier;
		}
		
		$p = array(
			'accountno'=>$this->_username,
			"pwd"=>$this->_password,
			"id"=>$identifier,
		);
		$uri = $this->getCheckMessageURI($p);
		$CI = &get_instance();
		$CI->load->library('curl');
		
		$str = $CI->curl->simple_get($uri);

		$res->responseString = $str;
		$res->provider = $this->getProviderId();

		log_message('debug','AccessYou_SMSProvider/send, responseString = '.nl2br(htmlentities($str)));


		$res->status = $this->translateStatus( (string) $str );
		
		return $res;
	}
	
	protected function translateStatus($state){
		
		if($state == 'finish'){
			return 'complete';
		}elseif($state == 'sending'){
			return 'sending';
		}elseif($state == 'pending'){
			return 'queueing';
		}
		return $state;
	}
	
	protected function getSendMessageURI($p){
		if(!empty($p['from']))
			return "http://api.accessyou.com/sms/sendsms-utf8-senderid.php?".http_build_query($p);

		return "http://api.accessyou.com/sms/sendsms-utf8.php?".http_build_query($p);
	}
	
	protected function getCheckMessageURI($p){
		return "http://api.accessyou.com/sms/getstatus.php?".http_build_query($p);
	}
}