<?php

$lang['redeems']  = 'Redeems';
$lang['missions'] = 'Missions';
$lang['requests'] = 'Requests';
$lang['types']    = 'Types';
$lang['property'] = 'Property';

$lang['male']   = 'Male';
$lang['female'] = 'Female';

$lang['featured'] = 'Featured';
$lang['yes']      = 'Yes';
$lang['no']       = 'No';
$lang['priority'] = 'Priority';

$lang['email']          = 'Email Address';
$lang['birth_date']     = 'Birthday';
$lang['gender']         = 'Gender';
$lang['mobile']         = 'Mobile No.';
$lang['history']        = 'History';
$lang['change_filter']  = 'Change Filter';
$lang['transcation']    = 'Transcation';
$lang['available_date'] = 'Available Date';
$lang['order_points']   = 'Order Points';
$lang['order_date']     = 'Order Date';
$lang['amount']         = 'Amount';
$lang['member']         = 'Member';
$lang['stock']          = 'Stock';
$lang['quota']          = 'Quota';
$lang['balance']        = 'Balance';

$lang['save']            = 'Save';
$lang['save_setting']    = 'Save Setting';
$lang['save_changes']    = 'Save Changes';
$lang['discard_changes'] = 'Discard Changes';
$lang['add']             = 'Add';
$lang['edit']            = 'Edit';
$lang['preview']         = 'Preview';
$lang['search']          = 'Search';
$lang['pending']         = 'Pending';
$lang['cancel']          = 'Cancel';
$lang['approve']         = 'Approve';
$lang['complete']        = 'Complete';
$lang['reject']          = 'Reject';
$lang['sign_in']         = 'Sign In';
$lang['sign_out']        = 'Sign Out';
$lang['change_password'] = 'Change Password';
$lang['profile']         = 'Profile';
$lang['my_profile']      = 'My Profile';
$lang['plan']            = 'Plan';
$lang['name']            = 'Name';
$lang['cover_image']     = 'Cover Image';
$lang['featured_image']  = 'Featured Image';
$lang['banner_image']    = 'Banner Image';
$lang['content']         = 'Content';
$lang['description']     = 'Description';
$lang['sys_identifier']  = 'System Identifier';

$lang['select_image']           = 'Select Image';
$lang['change_image']           = 'Change Image';
$lang['same_as_cover_image']    = 'Same as Cover Image';
$lang['same_as_featured_image'] = 'Same as Featured Image';
$lang['same_as_banner_image']   = 'Same as Banner Image';

$lang['home']                = 'Home';
$lang['transactions']        = 'Transactions';
$lang['sharing_title']       = 'Sharing Title';
$lang['sharing_caption']     = 'Sharing Caption';
$lang['sharing_description'] = 'Sharing Description';
$lang['no_result']           = 'No Result';
$lang['view_more']           = 'View More';
$lang['account']             = 'Account';
$lang['change_language']     = 'Change Language';
$lang['items']               = 'Items';
$lang['plans']               = 'Plans';
$lang['p_page_of']           = 'Page {$page} of {$total}';

$lang['income_level']        = 'Income Level';
$lang['edu_level']           = 'Education Level';
$lang['birth_date']          = 'Birth Date';
$lang['mobile']              = 'Mobile';
$lang['email']               = 'Email';
$lang['gender']              = 'Gender';
$lang['married_status']      = 'Married Status';
$lang['home_address']        = 'Home Address';
$lang['addr_street']         = 'Street';
$lang['addr_district']       = 'District';
$lang['not_available']       = 'Not Available';
$lang['not_available_short'] = 'N/A';
$lang['browse']              = 'Browse';
$lang['upload']              = 'Upload';
$lang['show_all']            = 'Show All';
$lang['status']              = 'Status';
$lang['back_to_list']        = 'Back to list';
$lang['subject'] = 'Subject';
$lang['import'] = 'Import';
$lang['export'] = 'Export';
$lang['other'] = 'Other';
$lang['since'] = 'Since';

$lang['def_locale'] = 'Default Language';

$lang['lang_en'] = 'English';
$lang['lang_zh'] = 'Chinese (Trad.)';
$lang['lang_cn'] = $lang['lang_zh-cn'] = 'Chinese (Smpl.)';
$lang['lang_jp'] = 'Japanese';
$lang['lang_kr'] = 'Korean';

$lang['error_validation'] = 'Validation Error.';
$lang['error_unique_email'] = '{email} is used.';
