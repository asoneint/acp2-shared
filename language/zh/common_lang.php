<?php

$lang['redeems']  = '獎賞';
$lang['missions'] = '任務';
$lang['requests'] = '請求';
$lang['types']    = '種類';
$lang['property'] = '屬性';

$lang['male']   = '男性';
$lang['female'] = '女性';

$lang['featured'] = '精選';
$lang['yes']      = '是';
$lang['no']       = '否';
$lang['priority'] = '優先次序';

$lang['email']          = '電郵地址';
$lang['birth_date']     = '出日日期';
$lang['gender']         = '性別';
$lang['mobile']         = '手提電話';
$lang['history']        = '過往紀錄';
$lang['transcation']    = '交易';
$lang['change_filter']  = '更換篩選條件';
$lang['available_date'] = '供應時間';
$lang['order_points']   = '交易點數';
$lang['order_date']     = '交易日期';
$lang['amount']         = '所需績分';
$lang['member']         = '會員';
$lang['stock']          = '貨存';
$lang['quota']          = '上限';
$lang['balance']        = '績分';

$lang['save']                   = '儲存';
$lang['save_setting']           = '儲存設定';
$lang['save_changes']           = '儲存修改項目';
$lang['discard_changes']        = '取消修改項目';
$lang['edit']                   = '修改';
$lang['preview']                = '預覽';
$lang['add']                    = '新增';
$lang['search']                 = '搜尋';
$lang['cancel']                 = '取消';
$lang['pending']                = '等待中';
$lang['approve']                = '允許';
$lang['reject']                 = '拒絕';
$lang['complete']               = '完成';
$lang['sign_in']                = '登入';
$lang['sign_out']               = '登出';
$lang['change_password']        = '更改密碼';
$lang['profile']                = '檔案';
$lang['my_profile']             = '我的檔案';
$lang['plan']                   = '方案';
$lang['name']                   = '名稱';
$lang['cover_image']            = '封面圖像';
$lang['featured_image']         = '精選圖像';
$lang['banner_image']           = '內文圖像';
$lang['content']                = '內文';
$lang['description']            = '簡介';
$lang['sys_identifier']         = '系統識別碼';
$lang['select_image']           = '選取圖片';
$lang['change_image']           = '更換圖片';
$lang['remove']                 = '刪除';
$lang['same_as_cover_image']    = '與封面圖像相同';
$lang['same_as_featured_image'] = '與精選圖像相同';
$lang['same_as_banner_image']   = '與內文圖像相同';

$lang['home']                = '首頁';
$lang['transactions']        = '交易';
$lang['sharing_title']       = '分享標題';
$lang['sharing_caption']     = '分享主旨';
$lang['sharing_description'] = '分享簡介';
$lang['no_result']           = '沒有結果';
$lang['view_more']           = '瀏覽更多';
$lang['account']             = '戶口';
$lang['change_language']     = '更換語言';
$lang['items']               = '項目';
$lang['plans']               = '計畫';
$lang['p_page_of']           = '在 {$total} 頁中的第 {$page} 頁';

$lang['income_level']        = '收入程度';
$lang['edu_level']           = '教育程度';
$lang['married_status']      = '婚姻狀態';
$lang['home_address']        = '家住地址';
$lang['addr_street']         = '街道';
$lang['addr_district']       = '地區';
$lang['not_available']       = '沒有可用';
$lang['not_available_short'] = '不提供';
$lang['browse']              = '瀏覽';
$lang['upload']              = '上傳';
$lang['show_all']            = '顯示全部';
$lang['status']              = '狀態';
$lang['back_to_list']        = '返回列表';
$lang['subject'] = '主題';
$lang['import'] = '匯入';
$lang['export'] = '匯出';
$lang['other'] = '其他';
$lang['since'] = '自從';

$lang['def_locale'] = '預設語言';

$lang['lang_en'] = '英文';
$lang['lang_zh'] = '中文(繁體)';
$lang['lang_cn'] = $lang['lang_zh-cn'] = '中文(簡體)';
$lang['lang_jp'] = '日本語';
$lang['lang_kr'] = '韓文';

$lang['error_validation'] = '驗證錯誤';
$lang['error_unique_email'] = '{email} 已被使用.';
