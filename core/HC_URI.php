<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class HC_URI extends CI_URI {

    function __construct() {
        parent::__construct();
    }
    
    var $_extension = false;   // The request extension name.  
  
    /** 
     * Explode the URI Segments. The individual segments will 
     * be stored in the $this->segments array.   
     * 
     * @access  private 
     * @return  void 
     */       
    function _explode_segments()  
    {  
        foreach(explode("/", preg_replace("|/*(.+?)/*$|", "\\1", $this->uri_string)) as $val)  
        {  
            // Filter segments for security  
            $val = trim($this->_filter_uri($val));  
              
            if ($val != '')  
                $this->segments[] = $val;  
        }
  
        $refLastSegment =& $this->segments[count($this->segments)-1];  
        
        if ( preg_match('/(.+)\.(\w+)$/', $refLastSegment, $matches) ) {  
            $refLastSegment = $matches[1];  
            $this->_extension = strtolower($matches[2]);  
        } 
    }  
  
    // --------------------------------------------------------------------  
      
    /**  
     * Fetch the file extension.  
     *   
     * If user request 'http://localhost/ci_test/index.php/control/index.xml',  
     * this will return 'xml'.                 
     *  
     * @access  public  
     * @return  string  
     */  
    function extension($val=FALSE)  
    {  
        if($val !== FALSE)
            $this->_extension = $val;
        return $this->_extension;  
    } 
    
    function is_extension($val=FALSE)  
    {  
        if(is_array($val))
            return in_array($this->_extension,$val);
        return $this->_extension == $val;  
    }
}
