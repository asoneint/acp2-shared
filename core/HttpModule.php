<?php 

class HttpModule extends HC_Module {

    function __construct(){
        parent::__construct();
    }
	
	var $supported_extensions = array(
		'html'=> array('', 'htm', 'html'),
		'view'=> array('', 'htm', 'html', 'js'),
		'asset'=> array('css', 'js'),
		'data'=> array('json', 'plist', 'xml'),
	);


    public function is_support_format($group='html'){
        if(!isset($this->supported_extensions[$group])) return FALSE;
        $group_exts = $this->supported_extensions[$group];

        return $this->uri->is_extension($group_exts);
    }

	public function output($vals, $default_format = 'json'){

		if($this->uri->is_extension(''))
			$this->uri->extension($default_format);

        $this->load->helper('data');
        
        if(empty($opts) || !is_array($opts))
            $opts = array();

        if($this->input->get_post('jscallback') !=NULL){

            $callback = $this->input->get('callback');
            if($callback!=null && strlen($callback)>0)
                $opts['callback'] = $callback;
            
            $script = $this->input->get('script');
            if($script == 'yes' || $script == 'true')
                $opts['script'] = TRUE;
                
            return json_output($vals,false,$opts);
        }
        
        if($this->uri->is_extension('json') ){
            
            $callback = $this->input->get('callback');
            if($callback!=null && strlen($callback)>0)
                $opts['callback'] = $callback;
            
            $script = $this->input->get('script');
            if($script == 'yes' || $script == 'true')
                $opts['script'] = TRUE;
                
            return json_output($vals,false,$opts);

        }elseif($this->uri->is_extension('xml') ){
            return xml_output($vals,false,$opts);

        }elseif($this->uri->is_extension('plist') ){
            return plist_output($vals,false,$opts);

        }
        return $this->_show_404('extension_not_matched');
	}



    
    public function error404($message){
        $this->output->set_header('HTTP/1.1 404 Page not found');

        // reset render view's extension
        $this->uri->extension('');

        if ($this->is_debug()) {
            header("Content-type: text/plain");
            print "Message: ".$message."\r\n";
            print"Segments: " . print_r($this->uri->segments, true) . "\r\n";
            print"Redirected Segments: " . print_r($this->uri->rsegments, true) . "\r\n";
            print"Backtrace: " . "\r\n" ; debug_print_backtrace() ; print "\r\n";
            return;
        }

        log_message('error','404 ERROR at '.uri_string().'. Message returned: '.$message.'');

        return $this->view('404', array(), 'blank');
    }

    public function view($view, $vals = false, $layout = false, $theme = false){

        $this -> load -> library('render');

        if (!is_array($vals)) {
            $vals = array();
        }
        
        $this->load->library('render');
        if( $this->is_debug() ){
            $this->render->compress_js = false;
        }

        if (!isset($vals['is_dialog'])) {
            $vals['is_dialog'] = false;
        }

        if (!$theme) {
            $theme = $this->config->item('theme');
        }

        if (!$layout) {
            $layout = 'default';
        }

        if (!$this->is_support_format('html')) {
            $req_ext   = $this->uri->extension();
            $view_path = NULL;

            if (empty($view_path)) {
                $view_path = 'themes/' . $theme . '/' . $view . '.' . $req_ext . EXT;
                if (!file_exists(APPPATH . 'views/' . $view_path)) {
                    $view_path = NULL;
                }
            }
            if (empty($view_path)) {
                $view_path = $view . '.' . $req_ext . EXT;
                if (!file_exists(APPPATH . 'views/' . $view_path)) {
                    $view_path = NULL;
                }
            }
            if (empty($view_path)) {
                $view_path = 'themes/' . $theme . '/' . $view . EXT;
                if (!file_exists(APPPATH . 'views/' . $view_path)) {
                    $view_path = NULL;
                }
            }
            if (empty($view_path)) {
                $view_path = $view . EXT;
                if (!file_exists(APPPATH . 'views/' . $view_path)) {
                    $view_path = NULL;
                }
            }
            if (!empty($view_path)) {
                if ($this->uri->is_extension('js')) {
                    $this->output->set_content_type('text/javascript');
                } elseif ($this->uri->is_extension('xml', 'plist')) {
                    $this->output->set_content_type('text/xml');
                } else {

                    $this->output->set_content_type('text/plain');
                }

                return $this->load->view($view_path, $vals);
            }
            return $this->_show_404();
        }

        $this->load->helper('form');

        $view_path = '';
        $view_theme_path = '';
        if (empty($view_path) && !empty($view)) {
            $theme_path = 'themes/' . $theme . '/';
            $view_theme_path = $theme_path;
            $view_path = 'themes/' . $theme . '/' . $view . EXT;
            if (!file_exists(APPPATH . 'views/' . $view_path)) {
                $view_path = NULL;
                $theme_path = '';
                $view_theme_path = '';
            }
        }
        if (empty($view_path) && !empty($view)) {
            $view_path = $view . EXT;
            if (!file_exists(APPPATH . 'views/' . $view_path)) {
                $view_path = NULL;
            }
        }

        $layout_path = '';

        if (empty($layout_path)) {
            $theme_path = 'themes/' . $theme . '/';
            $layout_path = 'themes/' . $theme . '/layouts/' . $layout . EXT;
            if (!file_exists(APPPATH . 'views/' . $layout_path)) {
                $layout_path = NULL;
                $theme_path = '';
            }
        }
        if (empty($layout_path)) {
            $theme_path = 'themes/' . $theme . '/';
            $layout_path = 'themes/' . $theme . '/' . $layout . EXT;
            if (!file_exists(APPPATH . 'views/' . $layout_path)) {
                $layout_path = NULL;
                $theme_path = '';
            }
        }
        if (empty($layout_path)) {
            $theme_path = 'themes/' . $theme . '/';
            $layout_path = 'themes/' . $theme . '/default' . EXT;
            if (!file_exists(APPPATH . 'views/' . $layout_path)) {
                $layout_path = NULL;
                $theme_path = '';
            }
        }
        if (empty($layout_path)) {
            $layout_path = 'layouts/' . $layout . EXT;
            if (!file_exists(APPPATH . 'views/' . $layout_path)) {
                $layout_path = NULL;
            }
        }
        if (empty($layout_path)) {
            $layout_path = $layout;
        }

        $vals['is_debug'] = $this->is_debug();
        $vals['view']      = $view;
        $vals['view_path'] = $view_path;
        $vals['theme_path'] = $theme_path;
        $vals['view_theme_path'] = $view_theme_path;
        $vals['theme']     = $theme;
        $vals['layout']    = $layout;

        $this->load->view($layout_path, $vals);
    }
}