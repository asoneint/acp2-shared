<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HC_Controller extends CI_Controller
{
	var $http;
	var $resource;

	function __construct(){

		if(!isset($_SERVER['REMOTE_ADDR']))
			$_SERVER['REMOTE_ADDR'] = '0.0.0.0';

		parent::__construct();

		$this->http = new HttpModule();

		$this->resource = new ResourceModule();

		$this->load->helper('render');
		$this->load->library('render');
		
		$this->_init_session();
		
		
		$req_sess_debug = $this->input->get('session-debug');
		if (!empty($req_sess_debug) ) {
			$this->config->set_item('debug_mode', $req_sess_debug);
			$this->session->set_userdata('debug_mode', $req_sess_debug);
		}elseif( isset($this->session) && $this->session->userdata('debug_mode') !== NULL){
			$this->config->set_item('debug_mode',$this->session->userdata('session-debug'));
		}
		

		$this -> load -> model('pref_model');

		// grap the system upload max size
		$sys_upload_max_size = ini_get('upload_max_filesize');
		if(strtoupper(substr($sys_upload_max_size, -1,1))=='M'){
			$sys_upload_max_size = intval(substr( $sys_upload_max_size,0, strlen($sys_upload_max_size)-1 ));
		}
		$this->config->set_item('sys_upload_max_size', $sys_upload_max_size);

		// get time zone setting
		$pref_timezone = $this->pref_model->item('timezone');

		if(!empty($pref_timezone)){
			$this->config->set_item('timezone', $pref_timezone);

			if(function_exists('date_default_timezone_set')){
				date_default_timezone_set($pref_timezone);	
			}
		}else{
			$pref_timezone = date_default_timezone_get();	
			$this->config->set_item('timezone', $pref_timezone);
		}
	}

	protected function _get_default_vals($action='index', $vals= array()){
		//$vals ['is_debug'] = $this->_is_debug();
		return $vals;
	}

	protected function _init_session(){
		// Feature: Native PHP Session
		$cfg=  array();
		if($this->input->get('token') != NULL){
			$cfg['token'] = $this->input->get('token');
		}
		$this->load->library('Native_session',$cfg,'session');
	}
	
	// return FALSE which is Allowed.
	public function _restrict($scope = NULL,$redirect=true){
		return FALSE;
	}
	
	public function _permission_denied($scope=NULL){
		return $this->_error(ERROR_MISSING_PERMISSION, ERROR_MISSING_PERMISSION_MSG, 401, compact('scope'));
	}
	
	public function _is_debug(){
		return $this->http->is_debug();
	}

	public function _is_ext($group='html'){
		return $this->http->is_support_format($group);
	}

	public function _is_extension($group='html'){
		return $this->http->is_support_format($group);
	}

	public function _system_error($code, $message = 'Unknown system error.', $status=200, $data = NULL){
		return $this->http->system_error($code, $message, $status, $data);
	}
	
	public function _api($vals, $default_format = 'json') {
		return $this->http->output($vals, $default_format); 
	}
	
	public function _error($code, $message = '', $status = 200, $data=NULL) {
		return $this->http->error($code, $message, $status, $data); 
	}

	public function _show_404($message = 'Un-specified 404 error.') {
		return $this->http->error404($message);
	}

	protected function _render($view, $vals = false, $layout = false, $theme = false) {
		return $this->http->view($view, $vals, $layout, $theme);
	}
}
